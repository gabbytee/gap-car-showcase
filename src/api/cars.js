import cars from '../data/cars.json';  

export default class ApiCars {
  static getList(makeFilter) {
    return new Promise(resolve => {
      setTimeout(() => {
        let carList = cars.carList;
        carList = filterByMake(carList, makeFilter);
        carList = sortByModel(carList);
        resolve(carList);
      }, 1000);
    });
  }
}

const filterByMake = (carList, makeFilter) => {
  let list = [...carList]; 
  if (!!makeFilter) {
    list = carList.filter((car) => {
      return car.make.toLowerCase().indexOf(makeFilter.toLowerCase()) >= 0;
    });
  }
  return list;
}

const sortByModel = (carList) => {
  return [...carList].sort((a,b) => {
      const x = a.model.toLowerCase();
      const y = b.model.toLowerCase();
      if (x < y) { return -1;}
      if (x > y) {return 1;}
      return 0;
  });
}
