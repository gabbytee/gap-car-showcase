import { takeLatest } from "redux-saga";
import { fork } from "redux-saga/effects";

import { fetchStart } from "./cars";

// main saga generators
export function* sagas() {
  yield [
    fork(takeLatest, 'FETCH_START', fetchStart),
  ];
}
