import { assert } from "chai";
import ui from "../../src/reducers/ui";

describe('UI reducer', () => {
  describe('FETCH_START', () => {
    it('should change isFetchingData property to true', () => {
      assert.include(
        ui({}, {
          type: 'FETCH_START'
        }),
        { isFetchingData: true }
      );
    });
  });

  describe('FETCH_READY', () => {
    it('should change isFetchingData property to false', () => {
      assert.include(
        ui({}, {
          type: 'FETCH_READY'
        }), 
        { isFetchingData: false }
      );
    });
  }); 

  describe('FILTER_SET', () => {
    it('should assign filter to corresponding property', () => {
      assert.include(
        ui({}, {
          type: 'FILTER_SET',
          filter: 'test'
        }), 
        { filter: 'test' }
      );
    });
  }); 

  describe('DETAILS_SHOW', () => {
    it('should assign carDetail and isDetailActive property to true', () => {
      const car = {
        id: 1,
        make: 'Audi',
        model: '100',
        year: 1985
      };
      assert.deepInclude(
        ui({}, {
          type: 'DETAILS_SHOW',
          car: car
        }), 
        { carDetail: car, isDetailActive: true }
      );
    });
  }); 

  describe('DETAILS_CLOSE', () => {
    it('should remove carDetail and set isDetailActive property to false', () => {
      assert.deepInclude(
        ui({}, {
          type: 'DETAILS_CLOSE'
        }),
        { carDetail: {}, isDetailActive: false }
      );
    });
  });  

  describe('COMPARE_ADD', () => {
    it('should add car to carsToCompare collection', () => {
      const car = {
        id: 1,
        make: 'Audi',
        model: '100',
        year: 1985
      };      
      assert.deepInclude(
        ui({ carsToCompare: [] }, {
          type: 'COMPARE_ADD',
          car: car
        }),
        { carsToCompare: [ car ] }
      );
    });
    it('should not add car if there are three in the list', () => {
      const cars = [{
        id: 1,
        make: 'Audi',
        model: '100',
        year: 1985
      },
      {
        id: 2,
        make: 'Volvo',
        model: '200',
        year: 1999
      },
      {
        id: 3,
        make: 'Chevrolet',
        model: '300',
        year: 2015
      }];
      const car = {
        id: 4,
        make: 'Mazda',
        model: '400',
        year: 2000
      };             
      assert.deepEqual(
        ui({ carsToCompare: cars }, {
          type: 'COMPARE_ADD',
          car: car
        }),
        { carsToCompare: cars }
      );
    });
    it('should not add car if it is already in the list', () => {
      const cars = [{
        id: 1,
        make: 'Audi',
        model: '100',
        year: 1985
      },
      {
        id: 2,
        make: 'Volvo',
        model: '200',
        year: 1999
      }];
            
      assert.deepEqual(
        ui({ carsToCompare: cars }, {
          type: 'COMPARE_ADD',
          car: cars[0]
        }),
        { carsToCompare: cars }
      );
    });          
  });    

  describe('COMPARE_REMOVE', () => {
    it('should remove car from carsToCompare collection', () => {
      const cars = [{
        id: 1,
        make: 'Audi',
        model: '100',
        year: 1985
      },
      {
        id: 2,
        make: 'Volvo',
        model: '200',
        year: 1999
      },
      {
        id: 3,
        make: 'Chevrolet',
        model: '300',
        year: 2015
      }];
      assert.deepEqual(
        ui({ carsToCompare: cars }, {
          type: 'COMPARE_REMOVE',
          car: cars[0]
        }),
        { carsToCompare: [cars[1], cars[2]] }
      );
    });
  });   

  describe('COMPARE_REMOVE_ALL', () => {
    it('should remove car from carsToCompare collection', () => {
      const cars = [{
        id: 1,
        make: 'Audi',
        model: '100',
        year: 1985
      },
      {
        id: 2,
        make: 'Volvo',
        model: '200',
        year: 1999
      },
      {
        id: 3,
        make: 'Chevrolet',
        model: '300',
        year: 2015
      }];
      assert.deepEqual(
        ui({ carsToCompare: [] }, {
          type: 'COMPARE_REMOVE_ALL'
        }),
        { carsToCompare: [] }
      );
    });
  });     
});
