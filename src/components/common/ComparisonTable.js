import React from "react";
import { Image, Table } from "react-bootstrap";
import StarRatingComponent from "react-star-rating-component";
import NumberFormat from "react-number-format";

export default class ComparisonTable extends React.Component {
  render() {
    const carsToCompare = this.props.carsToCompare;

    return (
        <Table responsive striped bordered condensed hover>
            <tbody>
                <tr>
                <td className="text-primary">&nbsp;</td>
                {
                    carsToCompare.map((car, i) => (
                    <td className="text-center text-muted">{i+1}</td>
                    ))
                }
                </tr>
                <tr>
                <td className="text-primary">Car</td>
                {
                    carsToCompare.map((car) => (
                    <td><Image src={`/media/thumbnails/${car._id}.jpg`} rounded responsive /></td>
                    ))
                }
                </tr>
                <tr>
                <td className="text-primary">Make</td>
                {
                    carsToCompare.map((car) => <td>{car.make}</td>)
                }
                </tr> 
                <tr>
                <td className="text-primary">Model</td>
                {
                    carsToCompare.map((car) => <td>{car.model}</td>)
                }
                </tr>
                <tr>
                <td className="text-primary">Year</td>
                {
                    carsToCompare.map((car) => <td>{car.year}</td>)
                }
                </tr>   
                <tr>
                <td className="text-primary">Fuel</td>
                {
                    carsToCompare.map((car) => <td>{car.fuel}</td>)
                }
                </tr>
                <tr>
                <td className="text-primary">Pass.</td>
                {
                    carsToCompare.map((car) => <td>{car.passengers}</td>)
                }
                </tr>
                <tr>
                <td className="text-primary">Make</td>
                {
                    carsToCompare.map((car) => <td>{car.make}</td>)
                }
                </tr>                                                                                                               
                <tr>
                <td className="text-primary">Rating</td>
                {
                    carsToCompare.map((car) => <td><StarRatingComponent name="rating" editing={false} starCount={5} value={car.rating} /></td>)
                }
                </tr>                                                                                                               
                <tr>
                <td className="text-primary">Price</td>
                {
                    carsToCompare.map((car) => <td><NumberFormat value={car.price} displayType='text' thousandSeparator={true} prefix={'$'} /></td>)
                }
                </tr>
            </tbody>
        </Table> 
    );
  }
}
