import { call, put } from "redux-saga/effects";
import ApiCars from "../api/cars";

// fetch the cars list
export function* fetchStart(action) {
  const cars = yield call(ApiCars.getList, action.filter);

  // save the cars in state
  yield put({
    type: 'FETCH_READY',
    cars: cars,
  });
}
