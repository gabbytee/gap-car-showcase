import assert from "assert";
import cars from "../../src/reducers/cars";

describe('Cars reducer', () => {
  describe('FETCH_READY', () => {
    it('should return a list of cars', () => {
      assert.deepEqual(
        cars({}, {
          type: 'FETCH_READY',
          cars: [{
            id: 1,
            make: 'Audi',
            model: '100',
            year: 1985
          }],
        }), [{
          id: 1,
          make: 'Audi',
          model: '100',
          year: 1985
        }]
      );
    });
  });
});
