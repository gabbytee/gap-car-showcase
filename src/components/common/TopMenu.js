import React from "react";
import { connect } from "react-redux";
import { Link, LinkContainer } from "react-router-bootstrap";
import {
    FormControl,
    MenuItem,
    Nav,
    Navbar, 
    NavDropdown
} from "react-bootstrap";

import SearchBox from "./SearchBox";

export class TopMenu extends React.Component {
  constructor(props) {
      super(props);

      this.clearAllClickHandler = this.clearAllClickHandler.bind(this);
  }

  clearAllClickHandler() {
    this.props.dispatch({
        type: 'COMPARE_REMOVE_ALL'
    });
  }

  render() {
    const carsToCompare = this.props.ui.carsToCompare;

    return (
        <Navbar bsStyle="inverse" fluid>
            <Navbar.Header>
                <Navbar.Brand>
                    <span>GAP Car Showcase</span>
                </Navbar.Brand>
                <Navbar.Toggle />
            </Navbar.Header>
            <Navbar.Collapse>
                <Nav>
                <NavDropdown title="Compare" id="model-compare-dropdown" className="visible-xs-block" >
                    <MenuItem onClick={this.clearAllClickHandler} disabled={!carsToCompare || carsToCompare.length<1}>Clear all</MenuItem>
                    <LinkContainer to="/compare">
                        <MenuItem disabled={!carsToCompare || carsToCompare.length<2}>Show Comparison</MenuItem>
                    </LinkContainer>
                </NavDropdown>
                </Nav>
                <Navbar.Form pullRight>
                    <SearchBox />
                </Navbar.Form>
            </Navbar.Collapse>
        </Navbar>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    ui: state.ui || {}
  };
}
export default connect(mapStateToProps)(TopMenu)