# GAP Car Showcase

Este proyecto es la prueba técnica práctica requerida, de acuerdo con las especificaciones dadas:

## Herramientas usadas

Para desarrollarla, se usaron las siguientes herramientas, entre otras:
* NodeJS 7 sobre NVM
* Yarn 0.24
* Webpack 1.14 + Dev Server 1.16
* React 15.4 + Redux 3.6
* react-router 3
* redux-saga 0.14
* react-bootstrap
* Y para las pruebas Mocha + Chai + Enzyme para componentes

(La totalidad de dependencias pueden comprobarse en el archivo package.json)

## Instalación

Para instalarlo, solo es necesario seguir los siguientes pasos:

1) Clonar este repositorio.
2) `yarn install`
3) `yarn start`
4) Abrir la aplicación en localhost:8080

Para correr las pruebas, se puede ejecutar la tarea `yarn test` o para hacerlo en modo sostenido, `yarn test-watch`. No se hizo mucho énfasis en las pruebas para los componentes por cuanto se aplicaron con especial dedicación sobre los reducers.

## Consideraciones especiales

* La aplicación de react-bootstrap requirió algunas maniobras especiales en ciertos componentes para permitir el envío de props de manera simple, lo cual complicó la posibilidad de hacer pruebas más a fondo de los componentes.
* Se probó la responsividad de la aplicación por medio de simulaciones en Chrome y Firefox, más no se verificó en IE por limitaciones de sistema operativo.
* El archivo JSON se encuentra en `src/data/cars.json` y para asemejar condiciones típicas de desarrollo y latencia, se implementó un _timeout_ de un segundo para la carga de datos, el cual se maneja por medio de la respectiva _saga_ y eventos de redux.
* Hay un demo online en: https://gap-car-showcase-6b3f4.firebaseapp.com/  

### Contacto
Gabriel Trujillo C. (joaqt23@gmail.com)