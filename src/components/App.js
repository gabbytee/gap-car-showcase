import React from "react";
import { connect } from "react-redux";
import Loader from "react-loader-advanced";

import CarDetails from "./common/CarDetails";
import TopMenu from "./common/TopMenu";

import "../stylesheets/main.scss";

export class App extends React.Component {
  componentWillMount() {
    // the first time we load the app, we need the car list
    this.props.dispatch({
      type: 'FETCH_START',
      filter: this.props.ui.filter
    });
  }

  render() {
    // show the loading state while we wait for the app to load
   const {cars, children, ui} = this.props;

    return (
      <Loader 
        show={this.props.ui.isFetchingData} 
        hideContentOnLoad={false} 
        contentBlur={5}
        message={'Fetching data'}
        backgroundStyle={{backgroundColor: 'white', opacity: 0.6}}>
        <CarDetails />
        <div>
            <TopMenu />
          <div>
            {children}
          </div>
          <div className="footer">
            <span>
              Gabriel Trujillo C. &copy; 2017
            </span>
          </div>
        </div>
      </Loader>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    cars: state.cars || [],
    ui: state.ui || {}
  };
}
export default connect(mapStateToProps)(App);