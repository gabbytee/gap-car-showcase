// ui reducer
const initialState = {
  filter: '',
  carDetail: {},
  carsToCompare: [],
  isCompareActive: false,
  isDetailActive: false,
  isFetchingData: false,
}

export default function ui(state = initialState, action) {
  switch (action.type) {
    case 'FETCH_START':
      return Object.assign({}, state, {isFetchingData: true});
    case 'FETCH_READY':
      return Object.assign({}, state, {isFetchingData: false});

    case 'FILTER_SET':
      return Object.assign({}, state, {filter: action.filter});

    case 'DETAILS_SHOW':
      return Object.assign({}, state, {carDetail: action.car, isDetailActive: true});
    case 'DETAILS_CLOSE':
      return Object.assign({}, state, {carDetail: {}, isDetailActive: false});

    case 'COMPARE_ADD':
      let carsToCompareAdd = [...state.carsToCompare];
      if (!!carsToCompareAdd && carsToCompareAdd.length <= 2 && carsToCompareAdd.findIndex((car) => { return car._id === action.car._id; }) == -1)
      {
        carsToCompareAdd = [...carsToCompareAdd, action.car];  
      }
      return Object.assign({}, state, {carsToCompare: carsToCompareAdd});
    case 'COMPARE_REMOVE':
      let carsToCompareRemove = [...state.carsToCompare];
      const index = carsToCompareRemove.findIndex((car) => { return car._id === action.car._id; });
      if (!!carsToCompareRemove && index > -1)
      {
        carsToCompareRemove = [...carsToCompareRemove.slice(0,index), ...carsToCompareRemove.slice(index + 1)];  
      }
      return Object.assign({}, state, {carsToCompare: carsToCompareRemove});  
    case 'COMPARE_REMOVE_ALL':
      let carsToCompareRemoveAll = [...state.carsToCompare];
      if (!!carsToCompareRemoveAll && carsToCompareRemoveAll.length > 0)
      {
        carsToCompareRemoveAll = [];  
      }
      return Object.assign({}, state, {carsToCompare: carsToCompareRemoveAll});             
    default:
      return state;
  }
}