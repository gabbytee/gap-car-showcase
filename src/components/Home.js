import React from "react";
import { Col, Grid, Row } from "react-bootstrap";

import CarListView from "./common/CarListView";
import ComparePanel from "./common/ComparePanel";

// Home page component
export default class Home extends React.Component {
  render() {
    const cars = this.props.cars;
  
    return (
      <Grid>
        <Row>
          <Col xsHidden sm={3} lg={2}>
            <ComparePanel />
          </Col>
          <Col sm={9} lg={10}>
            <CarListView />
          </Col>
        </Row>
      </Grid>
    );
  }
}
