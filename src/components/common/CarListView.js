import React from "react";
import { connect } from "react-redux";
import { Col, Glyphicon, Row } from "react-bootstrap";

import CarListItem from "./CarListItem";

export class CarListView extends React.Component {
  render() {
    const { cars } = this.props;

    let view = null;
    if (cars && cars.length > 0) {
        view = (
            <div className="car-list-view">
                <Row className="container car-list-view-header">
                    <Col className="pull-left" xs={6} sm={4}>
                        <span><Glyphicon glyph="sort-by-attributes" />{' '}<em>Sorted by: <strong>Model</strong></em></span>
                    </Col>
                    <Col>
                        <span className="text-muted">Displaying {cars.length} match{cars.length > 1 ? 'es' : ''}</span>
                    </Col>
                </Row>            
                <Row>
                    {
                        cars.map((car) => <CarListItem car={car} key={car._id} />)
                    } 
                </Row>
            </div>           
        );
    }
    else {
        view = (
            <div className="text-center">
                <h4>No results found</h4>
            </div>
        );
    }

    return (
        <div>{view}</div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    cars: state.cars || []
  };
}
export default connect(mapStateToProps)(CarListView);
