import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router";
import { 
  Button, 
  Glyphicon, 
  Grid, 
  Row 
} from "react-bootstrap";

import TopMenu from "./common/TopMenu";
import ComparisonTable from "./common/ComparisonTable";

import "../stylesheets/main.scss";

export class Compare extends React.Component {

  render() {
   const carsToCompare = this.props.ui.carsToCompare;

    return (
      <div>
        <TopMenu />
        <div>
          <Grid>
            <Row>
              <Link to={'/'}>
                <Button bsStyle="link"><Glyphicon glyph="chevron-left"/>{' '}Back to Showcase</Button>
              </Link>
            </Row>
            <Row className="container">
              <ComparisonTable carsToCompare={carsToCompare} />             
            </Row>
          </Grid>
        </div>
        <div className="footer">
          <span>
            Gabriel Trujillo C. &copy; 2017
          </span>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    ui: state.ui || {}
  };
}
export default connect(mapStateToProps)(Compare);