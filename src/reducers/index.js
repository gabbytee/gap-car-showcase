import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";
import cars from "./cars";
import ui from "./ui";

export const reducers = combineReducers({
  routing: routerReducer,
  cars: cars,
  ui: ui,
});
