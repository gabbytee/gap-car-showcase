import React from "react";
import { connect } from "react-redux";
import ReactDOM from "react-dom";
import { 
    Button, 
    Col, 
    Glyphicon,
    Row,
    Thumbnail 
} from "react-bootstrap";
import NumberFormat from "react-number-format";

export class CarListItem extends React.Component {
  componentDidMount() {
      // Needed this hack since react-bootstrap doesn't handle the click event for Thumbnail components
      const a = ReactDOM.findDOMNode(this).querySelector('a.thumbnail:not(.car-list-item-add-compare');
      a.addEventListener('click', (event) => {
          event.preventDefault();
          const selectedCar = this.props.cars.find((car) => {
            return car._id === a.dataset.id;
          });
          this.props.dispatch({
              type: 'DETAILS_SHOW',
              car: selectedCar
          })
      })
  }

  render() {
    const car = this.props.car;
    return (
        <Col xs={6} sm={4} md={3}>
            <Thumbnail href="#" data-id={car._id} src={`/media/thumbnails/${car._id}.jpg`} alt={`${car.make} ${car.model} ${car.year}`}>
                <h4 className="text-primary">{car.model}</h4>
                <p>{`${car.make} | ${car.year}`}</p>
                <p className="text-right">
                    <strong>
                        <NumberFormat 
                            value={car.price} 
                            displayType='text' 
                            thousandSeparator={true} 
                            prefix={'$'} />
                    </strong>
                </p>
            </Thumbnail>
        </Col>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    cars: state.cars || [],
    ui: state.ui || {}
  };
}
export default connect(mapStateToProps)(CarListItem);