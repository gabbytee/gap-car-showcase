// cars reducer
export default function cars(state = {}, action) {
  switch (action.type) {
    case 'FETCH_READY':
      return action.cars;
    default:
      return state;
  }
}