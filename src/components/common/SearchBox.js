import React from "react";
import { connect } from "react-redux";
import {
    Button,
    FormControl, 
    FormGroup, 
    InputGroup, 
    Glyphicon
} from "react-bootstrap";

export class SearchBox extends React.Component {
  constructor(props) {
      super(props);
      this.keyUpHandler = this.keyUpHandler.bind(this);
      this.searchClickHandler = this.searchClickHandler.bind(this);
      this.searchChangeHandler = this.searchChangeHandler.bind(this);
  }

  keyUpHandler(event) {
    if (event.keyCode !== 13) return;
    event.preventDefault();
    this.props.dispatch({
        type: 'FETCH_START',
        filter: this.props.ui.filter
    });
  }

  searchChangeHandler(event) {
    const val = event.target.value.trim();
    this.props.dispatch({
       type: 'FILTER_SET',
       filter: val
    })
  }

  searchClickHandler() {
    this.props.dispatch({
        type: 'FETCH_START',
        filter: this.props.ui.filter
    });    
  }
  
  render() {
    return (
        <FormGroup bsSize="small">
            <InputGroup>
                <FormControl type="text" 
                    placeholder="Filter by make..." 
                    onChange={this.searchChangeHandler} 
                    onKeyUp={this.keyUpHandler}
                    value={this.props.ui.filter} />
                <InputGroup.Button>
                    <Button bsSize="small" onClick={this.searchClickHandler}><Glyphicon glyph="search" /></Button>
                </InputGroup.Button>
            </InputGroup>                        
        </FormGroup>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    cars: state.cars || [],
    ui: state.ui || {},
  };
}
export default connect(mapStateToProps)(SearchBox);                  