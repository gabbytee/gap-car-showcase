import React from "react";
import { shallow } from "enzyme";
import assert from "assert";
import ComparisonTable from "../../src/components/common/ComparisonTable";

// unit tests for the Menu component
describe('Comparison Table component', () => {
  describe('render()', () => {
    it('should render the component', () => {
      const cars = [{
        _id: 1,
        make: 'Audi',
        model: '100',
        year: 1985
      },
      {
        _id: 2,
        make: 'Volvo',
        model: '200',
        year: 1999
      },
      {
        _id: 3,
        make: 'Chevrolet',
        model: '300',
        year: 2015
      }];        
      const props = { carsToCompare: cars };
      const wrapper = shallow(<ComparisonTable {...props} />);
      assert.equal(wrapper.length, 1);
    });
  });
});