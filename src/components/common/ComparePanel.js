import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router";
import { 
    Button, 
    Col,
    Glyphicon,
    Panel, 
    Row 
} from "react-bootstrap";

import CompareItem from "./CompareItem";

export class ComparePanel extends React.Component {
  constructor(props) {
    super(props);

    this.handleRemoveAllCompare = this.handleRemoveAllCompare.bind(this);
  }

  handleRemoveAllCompare() {
    this.props.dispatch({
        type: 'COMPARE_REMOVE_ALL'
    })
  }

  render() {
    const carsToCompare = this.props.ui.carsToCompare;
      
    let button = null;
    let compareButton = null;
    let panelContent = (
        <span className="text-center text-muted"><em>No models selected</em></span>
    );
    
    if (!!carsToCompare && carsToCompare.length > 1) {
        compareButton = (
            <Col className="text-center">
                <Link to={'compare'}>
                    <Button bsStyle="primary" href="/compare"><Glyphicon glyph="list" />{' '}Compare models</Button>
                </Link>
            </Col>
        );
    }

    if (!!carsToCompare && carsToCompare.length > 0) {
        button = (
            <Button bsSize="xsmall" bsStyle="link" onClick={this.handleRemoveAllCompare}>Clear</Button>
        );
        panelContent = (
            <div>
                {
                    carsToCompare.map((car) => <CompareItem car={car} key={`${car._id}-${car.model}`} />)
                }
                <Row>
                    <Col className="text-center">
                        <span>&nbsp;</span>
                    </Col>
                </Row>            
                <Row>
                {compareButton}
                </Row>
            </div>
        );
    }

    const panelHeader = (
        <Row className="panel-header-row">
            <Col className="pull-left"><strong>Compare List</strong></Col>
            <Col className="pull-right">
                {button}
            </Col>
        </Row>
    );

    return (
        <Panel header={panelHeader} bsStyle="primary">
            {panelContent}
        </Panel>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    ui: state.ui || {}
  };
}
export default connect(mapStateToProps)(ComparePanel)