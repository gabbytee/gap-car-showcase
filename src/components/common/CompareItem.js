import React from "react";
import { connect } from "react-redux";
import { 
    Button, 
    Col,
    Glyphicon,
    Panel, 
    Row 
} from "react-bootstrap";

export class CompareItem extends React.Component {
  constructor(props) {
      super(props);

      this.clickHandler = this.clickHandler.bind(this);
  }

  clickHandler(event) {
    this.props.dispatch({
        type: 'COMPARE_REMOVE',
        car: this.props.car 
    })
  }

  render() {
    const car = this.props.car;

    return (
        <Row className="panel-row"> 
            <Col className="pull-left">{car.model}</Col>
            <Col className="pull-right">
                <Button 
                    type="button" 
                    bsClass="close" 
                    aria-label="Close"
                    onClick={this.clickHandler}>
                    <span aria-hidden="true">&times;</span>
                </Button>
            </Col>
        </Row>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    ui: state.ui || {}
  };
}
export default connect(mapStateToProps)(CompareItem)