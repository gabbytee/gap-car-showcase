import React from "react";
import { connect } from "react-redux";
import { 
  Button,
  Clearfix,
  Col,
  Glyphicon,
  Grid, 
  Image,
  Modal, 
  Row 
} from "react-bootstrap";
import StarRatingComponent from "react-star-rating-component";
import NumberFormat from "react-number-format";

export class CarDetail extends React.Component {
  constructor(props) {
    super(props);

    this.hideHandler = this.hideHandler.bind(this);
    this.compareAddClickHandler = this.compareAddClickHandler.bind(this);
    this.compareRemoveClickHandler = this.compareRemoveClickHandler.bind(this);
 }

  hideHandler() {
    this.props.dispatch({ type: 'DETAILS_CLOSE' });
  }

  compareAddClickHandler() {
    this.props.dispatch({
      type: 'COMPARE_ADD',
      car: this.props.ui.carDetail
    });
  }

  compareRemoveClickHandler() {
    this.props.dispatch({
      type: 'COMPARE_REMOVE',
      car: this.props.ui.carDetail
    });
  }

  render() {
    const car = this.props.ui.carDetail;

    let button = (
      <Button
        disabled={this.props.ui.carsToCompare.length == 3}
        bsStyle="primary" 
        className="pull-left" 
        onClick={this.compareAddClickHandler}>
        <Glyphicon glyph="plus" />{' '}Add to compare
      </Button>
    );
    if (!!this.props.ui.carsToCompare.find((c) => { return c._id === car._id; })) {
      button = (
        <Button bsStyle="warning" className="pull-left" onClick={this.compareRemoveClickHandler}>
          <Glyphicon glyph="minus" />{' '}Remove from compare
        </Button>      
      );
    }

    return (
      <Modal show={this.props.ui.isDetailActive} onHide={this.hideHandler}>
        <Modal.Header closeButton>
          <Modal.Title>{car.description}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Grid className="container">
            <Row>
              <Col xs={8} sm={7} md={5} lg={4}>
                <Image src={`/media/${car._id}.jpg`} rounded responsive />
                <div  className="text-center">
                  <h3>
                    <NumberFormat 
                      value={car.price} 
                      displayType='text' 
                      thousandSeparator={true} 
                      prefix={'$'} />
                  </h3>
                  <StarRatingComponent name="rating" editing={false} starCount={5} value={car.rating} /><br />
                  <span>{`${car.reviews} review${car.reviews > 1 ? 's' : ''}`}</span>
                </div>
              </Col>
              <Col xs={4} sm={5} md={7} lg={8}>
                <p><span className="detail-field-name">Make:{' '}</span><br /><span>{car.make}</span></p>
                <p><span className="detail-field-name">Model:{' '}</span><br /><span>{car.model}</span></p>
                <p><span className="detail-field-name">Year:{' '}</span><br /><span>{car.year}</span></p>
                <p><span className="detail-field-name">Fuel:{' '}</span><br /><span>{car.fuel}</span></p>
                <p><span className="detail-field-name">Passengers:{' '}</span><br /><span>{car.passengers}</span></p>
              </Col>
              <Clearfix visibleXsBlock>&nbsp;</Clearfix>
            </Row>
          </Grid>
        </Modal.Body>
        <Modal.Footer>
          {button}
          <Button className="pull-right" onClick={this.hideHandler}>Close</Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    cars: state.cars || [],
    ui: state.ui || {}
  };
}
export default connect(mapStateToProps)(CarDetail);